<?php

namespace AppBundle\Entity;

/**
 * Categorias
 */
class Categorias
{
    /**
     * @var string
     */
    private $nombre;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $tareas;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tareas = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Categorias
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add tarea
     *
     * @param \AppBundle\Entity\Tareas $tarea
     *
     * @return Categorias
     */
    public function addTarea(\AppBundle\Entity\Tareas $tarea)
    {
        $this->tareas[] = $tarea;

        return $this;
    }

    /**
     * Remove tarea
     *
     * @param \AppBundle\Entity\Tareas $tarea
     */
    public function removeTarea(\AppBundle\Entity\Tareas $tarea)
    {
        $this->tareas->removeElement($tarea);
    }

    /**
     * Get tareas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTareas()
    {
        return $this->tareas;
    }
}


<?php

namespace AppBundle\Entity;

/**
 * Tareas
 */
class Tareas
{
    /**
     * @var string
     */
    private $detalle;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $categorias;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->categorias = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set detalle
     *
     * @param string $detalle
     *
     * @return Tareas
     */
    public function setDetalle($detalle)
    {
        $this->detalle = $detalle;

        return $this;
    }

    /**
     * Get detalle
     *
     * @return string
     */
    public function getDetalle()
    {
        return $this->detalle;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add categoria
     *
     * @param \AppBundle\Entity\Categorias $categoria
     *
     * @return Tareas
     */
    public function addCategoria(\AppBundle\Entity\Categorias $categoria)
    {
        $this->categorias[] = $categoria;

        return $this;
    }

    /**
     * Remove categoria
     *
     * @param \AppBundle\Entity\Categorias $categoria
     */
    public function removeCategoria(\AppBundle\Entity\Categorias $categoria)
    {
        $this->categorias->removeElement($categoria);
    }

    /**
     * Get categorias
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategorias()
    {
        return $this->categorias;
    }
}


/*
Created		10/06/2017
Modified		10/06/2017
Project		
Model		
Company		
Author		
Version		
Database		mySQL 5 
*/


drop table IF EXISTS categorias_tareas;
drop table IF EXISTS categorias;
drop table IF EXISTS tareas;


Create table tareas (
	id Bigint NOT NULL AUTO_INCREMENT,
	detalle Varchar(200) NOT NULL,
 Primary Key (id)) ENGINE = InnoDB;

Create table categorias (
	id Int NOT NULL AUTO_INCREMENT,
	nombre Varchar(50) NOT NULL,
	UNIQUE (nombre),
 Primary Key (id)) ENGINE = InnoDB;

Create table categorias_tareas (
	tareas_id Bigint NOT NULL,
	categorias_id Int NOT NULL,
 Primary Key (tareas_id,categorias_id)) ENGINE = InnoDB;


Alter table categorias_tareas add Foreign Key (tareas_id) references tareas (id) on delete  restrict on update  restrict;
Alter table categorias_tareas add Foreign Key (categorias_id) references categorias (id) on delete  restrict on update  restrict;


/* Users permissions */


